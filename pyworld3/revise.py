# File revise.py Functions used to change some variables and table,
# in order to generate more scenarios.

import json
import os

from pyworld3 import World3

def change_ytables_in_dict(tables_to_change, tables):
    """
    Change the values indicated in tables_to_change from the dictionnary table.

    Parameters
    ----------
    tables_to_change: dict(string, list(float))
        All y_name: y_values needed to change
        New y values, must be of the same length as the old one
    tables: dict()
        Dictionnary of the form of the function_table_world3.json

    Returns
    -------
    tables: dict()
        Dictionnary with modified values
    """
    for y_name, y_values in tables_to_change.items():
        changed = False
        for table in tables:
            if table['y.name'] == y_name:
                changed  = True
                if len(y_values) == len(table['y.values']):
                    table['y.values'] = list(y_values)
                else:
                    print(f"y values {y_name} of length {len(y_values)} is not the same as older ones {len(table['y.values'])}, change didnt occur")
                    # TODO: Raise an exception
        if not changed:
            print(f'Variable {y_name} not found')

    return tables

def write_new_table(tables_to_change, new_json_name, json_file=None):
    """
    Write a new json_file with the values with changes indicated in tables_to_change.

    Parameters
    ----------
    tables_to_change: dict(string, list(float))
        All y_name: y_values needed to change
        New y values, must be of the same length as the old one
    new_json_name: string
        New json file
    json_file: string
        Original json file, by default functions_table_world3.json

    Returns
    -------
    string
        name of new json file
    """
    if json_file is None:
        json_file = "functions_table_world3.json"
        json_file = os.path.join(os.path.dirname(__file__), json_file)
    with open(json_file) as fjson:
        tables = json.load(fjson)

    tables = change_ytables_in_dict(tables_to_change, tables)

    with open(new_json_name, "w+") as json_file:
        json.dump(tables, json_file)

def change_world(constants=None, tables_to_change=None, global_cst=None,
                    new_json_name="new_json_table.json", json_file=None):
    """
    Change appropriate constants and tables and run a simulation.

    Parameters
    ----------
    constants: dict(string, float)
        All cst_name: cst_value need to change
    tables_to_change: dict(string, list(float))
        All y_name: y_values needed to change

    Returns
    -------
    World with appropriate parameters
    """
    if constants is None:
        constants = dict()
    if tables_to_change is None:
        tables_to_change = dict()
    if global_cst is None:
        global_cst = dict()

    new_json_file = write_new_table(tables_to_change, new_json_name, json_file)

    w = World3(**global_cst)
    w.init_world3_constants(**constants)
    w.init_world3_variables()
    w.set_world3_table_functions(new_json_name)
    w.set_world3_delay_functions()

    return w
