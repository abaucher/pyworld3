import matplotlib.pyplot as plt
import numpy as np

from pyworld3.revise import change_world
from pyworld3.utils import plot_world_with_scales

dic_cst = dict()
dic_fun = dict()


# ---------------------------------------------------------------------------- #
w = change_world(dic_cst, dic_fun)
w.run_world3()
plot_world_with_scales(w, title="7-7 Global standart")
plt.show()

# ---------------------------------------------------------------------------- #
dic_fun = {"FCAOR2" : [1,   0.2,  0.1,   0.05,   0.05,  0.05,0.05, 0.05, 0.05, 0.05, 0.05],
            "LYMAP2" : [1,  1, 0.98, 0.95]
}
dic_cst = {"nruf2" : 0.125,
           "ppgf2" : 0.1
}
dic_scales = {"IOPC" : [0, 2e3]}

w = change_world(dic_cst, dic_fun)
w.run_world3()
plot_world_with_scales(w, dic_scales, title="7-20 Ressource, pollution technologies")
plt.show()
